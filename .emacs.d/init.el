;;; package --- Summary
;;; Commentary:
;;;-----------------------------------------------------------------------------
;;; Code:
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))
(require 'diminish)
(require 'bind-key)

;;;-----------------------------------------------------------------------------
;;; My functions
;;;-----------------------------------------------------------------------------
(defun my-expand-file-name-at-point ()
  "Use 'hippie-expand' to expand the file-name."
  (interactive)
  (setq hippie-expand-try-functions-list '(try-expand-dabbrev
                                            try-expand-dabbrev-all-buffers
                                            try-expand-dabbrev-from-kill
                                            try-complete-file-name-partially
                                            try-complete-file-name
                                            try-expand-all-abbrevs
                                            try-expand-list
                                            try-expand-line
                                            try-complete-lisp-symbol-partially
                                            try-complete-lisp-symbol))

  (let ((hippie-expand-try-functions-list '(try-complete-file-name-partially
                                             try-complete-file-name)))
    (call-interactively 'hippie-expand)))


(defsubst hook-into-modes (func &rest modes)
          "Macro to hook into modes.  FUNC -- any function, MODES -- any mode."
          (dolist (mode-hook modes) (add-hook mode-hook func)))


(defun latex-auto-fill ()
  "Turn on auto-fill for LaTeX mode."
  (turn-on-auto-fill)
  (setq-default fill-column 80))


(defun comment-auto-fill ()
  "Comment auto-fill."
  (setq-local comment-auto-fill-only-comments t)
  (setq-default fill-column 80)
  (auto-fill-mode 1))


(hook-into-modes #'comment-auto-fill
                 'prog-mode-hook)

;;;-----------------------------------------------------------------------------
;;; General key bindings
;;;-----------------------------------------------------------------------------
(global-set-key (kbd "M-["  ) 'insert-pair)
(global-set-key (kbd "M-{"  ) 'insert-pair)
(global-set-key (kbd "M-("  ) 'insert-pair)
(global-set-key (kbd "M-\"" ) 'insert-pair)
(global-set-key (kbd "M-\'" ) 'insert-pair)
(global-set-key (kbd "M-)"  ) 'delete-pair)

;;;-----------------------------------------------------------------------------
;;; General minor mode settings
;;;-----------------------------------------------------------------------------
;; (global-set-key (kbd "C-M-/") 'my-expand-file-name-at-point)

(defalias 'yes-or-no-p 'y-or-n-p)

(put 'erase-buffer 'disabled nil)

(setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t))
      backup-directory-alist `((".*" . ,temporary-file-directory)))

;;;-----------------------------------------------------------------------------
;; ;;; Dependent libraries:
;;;-----------------------------------------------------------------------------

(use-package dash   :ensure t :defer t)
(use-package ctable :ensure t :defer t)
(use-package deferred   :ensure t :defer t)
(use-package epc    :ensure t :defer t)
(use-package epl    :ensure t :defer t)
(use-package f      :ensure t :defer t)
(use-package fuzzy  :ensure t :defer t)
(use-package popup  :ensure t :defer t)
(use-package seq    :ensure t :defer t)
(use-package spinner    :ensure t :defer t)
(use-package hydra  :ensure t :defer t)
(use-package ag         :ensure t :defer t)
(use-package s          :ensure t :defer t)
(use-package let-alist  :ensure t)
(use-package pythonic   :ensure t :defer t)
;;;-----------------------------------------------------------------------------
;;; Asthetics Customization
;;;-----------------------------------------------------------------------------

(setq inhibit-startup-message t)
(menu-bar-mode 0)
(scroll-bar-mode -1)
(tool-bar-mode 0)
(put 'narrow-to-region 'disabled nil)
(set-face-attribute 'default nil
		    :family "YaHei Consolas Hybrid"
                    :foundry "MS"
                    :height 98
                    :width 'normal
                    :weight 'normal
                    :slant 'normal)


(use-package soothe-theme
  :if window-system
  :ensure t)


(use-package rainbow-mode
  :ensure t
  :defer t)


;; (use-package github-theme
;;  :ensure t)


(use-package powerline
  :ensure t
  :config
  (powerline-default-theme))


(use-package airline-themes
  :ensure t
  :config (load-theme 'airline-serene t))


(use-package beacon
  :ensure t
  :diminish beacon-mode
  :config
  (beacon-mode 1))


;; (use-package window-purpose
;;   :ensure t
;;   :config
;;   (purpose-mode)
;;   (add-to-list 'purpose-user-mode-purposes '(python-mode . py))
;;   (add-to-list 'purpose-user-mode-purposes '(inferior-python-mode . py-repl))
;;   (purpose-compile-user-configuration))


(use-package golden-ratio
  :disabled t
  :ensure t
  :config
  (golden-ratio-mode 1))

;;;-----------------------------------------------------------------------------
;; Packages --- list of packages for emacs using use-package
;; Commentary:
;;;-----------------------------------------------------------------------------
(use-package linum
  :config (global-linum-mode 1))


(use-package ido
  :config
  (ido-mode t))


(use-package smex
  :ensure t
  :config
  (smex-initialize)
  (global-set-key (kbd "M-x") 'smex)
  (global-set-key (kbd "C-c C-c M-x") 'execute-extended-command))


(use-package dumb-jump
  :bind (("M-g o" . dumb-jump-go-other-window)
         ("M-g j" . dumb-jump-go))
  :config (setq dumb-jump-selector 'ivy)
  :ensure)


(use-package restart-emacs
  :ensure t
  :bind* (("C-x M-c" . restart-emacs)))


(use-package diminish
  :ensure t
  :demand t
  :diminish auto-fill-function)


(use-package undo-tree
  :ensure t
  :diminish undo-tree-mode
  :config
  (global-undo-tree-mode))


(use-package macrostep
  :ensure t
  :bind (:map emacs-lisp-mode-map
              ("C-c C-e" . macrostep-expand)))


(use-package hippie-exp
  :bind (("M-/" . hippie-expand))
  :config
  (setq hippie-expand-try-functions-list '(try-expand-dabbrev
                                           try-expand-dabbrev-all-buffers
                                           try-expand-dabbrev-from-kill
                                           try-complete-file-name-partially
                                           try-complete-file-name
                                           try-expand-all-abbrevs
                                           try-expand-list
                                           try-expand-line
                                           try-complete-lisp-symbol-partially
                                           try-complete-lisp-symbol)))


(use-package flyspell
  :defer 15
  :diminish flyspell-mode
  :bind (("<f8>" . ispell-word)
         ("C-<f8>" . flyspell-check-previous-highlighted-word)
         ("M-<f8>" . flyspell-check-next-highlighted-word))
  :preface
  (defun flyspell-check-next-highlighted-word ()
    "Custom function to spell check next highlighted word"
    (interactive)
    (flyspell-goto-next-error)
    (ispell-word)
    )
  :init
  (setq ispell-program-name "aspell")
  (setq ispell-dictionary "english")
  (setq flyspell-issue-message-flag nil)
  (hook-into-modes #'flyspell-mode
                   'LaTeX-mode-hook)
  (hook-into-modes #'flyspell-buffer
                   'LaTeX-mode-hook)
  (hook-into-modes #'flyspell-prog-mode
                   'prog-mode-hook))


(use-package flycheck
  :ensure t
  :diminish flycheck-mode
  :defer 2
  :bind (("<f7>"     . flycheck-list-errors)
         ("C-c <f7>"     . flycheck-next-error)
         ("C-c   p <f7>" . flycheck-previous-error))
  :init
  (global-flycheck-mode))


(use-package flycheck-checkbashisms
  :ensure t
  :config
  (flycheck-checkbashisms-setup))


(use-package flycheck-haskell
  :ensure t
  :config
  (flycheck-haskell-setup))


(use-package abbrev
  :defer t
  :diminish abbrev-mode
  :init
  (dolist (hook '(prog-mode-hook
                  emacs-lisp-mode-hook
                  text-mode-hook))
    (add-hook hook (lambda () (abbrev-mode 1)))))


(use-package yasnippet
  :disabled t
  :demand t
  ;; :defer 10
  :diminish yas-minor-mode
  :commands (yas-expand yas-minor-mode)
  :functions (yas-guess-snipper-directories yas-tab)
  :defines (yas-guessed-modes)
  :mode ("/\\.emacs\\.d/snippets/" . snippet-mode)
  :preface
  (defun abort-company-or-yas ()
    (interactive)
    (if (null company-candidates)
        (yas-abort-snippet)
      (company-abort)))

  (defun tab-complete-or-next-field ()
    (interactive)
    (if (or (not yas-minor-mode)
            (null (do-yas-expand)))
        (if company-candidates
            (company-complete-selection)
          (if (check-expansion)
              (progn
                (company-manual-begin)
                (if (null company-candidates)
                    (progn
                      (company-abort)
                      (yas-next-field))))
            (yas-next-field)))))

  :bind (:map yas-minor-mode-map
              ([tab] . nil)
              ("TAB" . nil)
              :map yas-keymap
              ([tab] . tab-complete-or-next-field)
              ("TAB" . tab-complete-or-next-field)
              ("M-n" . yas-next-field)
              ("C-g" . abort-company-or-yas))
  :config
  ;; (yas-load-directory "~/.emacs.d/snippets/")
  (yas-global-mode 1))


(use-package evil
  :ensure t
  :bind (:map
         evil-insert-state-map
         ([S-left]  .   windmove-left)
         ([S-right] .   windmove-right)
         ([S-up]    .   windmove-up)
         ([S-down]  .   windmove-down)
         ("SPC"     .   nil)
         :map
         evil-normal-state-map
         (";" . evil-ex)
         (":"   .   evil-repeat-find-char)
         :map
         evil-motion-state-map
         ([S-left]  .   windmove-left)
         ([S-right] .   windmove-right)
         ([S-up]    .   windmove-up)
         ([S-down]  .   windmove-down)
         ("["           .   evil-backward-section-begin)
         ("]"           .   evil-forward-section-begin))
  :init
  (setq evil-insert-state-cursor '((bar . 1) "white")
        evil-visual-state-cursor '(box "dark orange")
        evil-normal-state-cursor '(box "white"))
  :config
  (evil-mode 1))


(use-package evil-leader
  :ensure t
  :preface
  (defun toggle-window-split ()
    (interactive)
    (if (= (count-windows) 2)
        (let* ((this-win-buffer (window-buffer))
               (next-win-buffer (window-buffer (next-window)))
               (this-win-edges (window-edges (selected-window)))
               (next-win-edges (window-edges (next-window)))
               (this-win-2nd (not (and (<= (car this-win-edges)
                                           (car next-win-edges))
                                       (<= (cadr this-win-edges)
                                           (cadr next-win-edges)))))
               (splitter
                (if (= (car this-win-edges)
                       (car (window-edges (next-window))))
                    'split-window-horizontally
                  'split-window-vertically)))
          (delete-other-windows)
          (let ((first-win (selected-window)))
            (funcall splitter)
            (if this-win-2nd (other-window 1))
            (set-window-buffer (selected-window) this-win-buffer)
            (set-window-buffer (next-window) next-win-buffer)
            (select-window first-win)
            (if this-win-2nd (other-window 1))))))
  :config
  (global-evil-leader-mode)
  (evil-leader/set-leader ",")
  (evil-leader/set-key
    "e" 'find-file
    "b" 'switch-to-buffer
    "k" 'kill-this-buffer
    "s" 'split-window-horizontally
    "p" 'sp-up-sexp
    "f" 'ffap
    "1" 'delete-other-windows
    "o" 'other-window
    "h" 'toggle-window-split
    "x" 'bookmark-jump))


(use-package evil-nerd-commenter
  :ensure t
  :config
  (evil-leader/set-key
    "ci" 'evilnc-comment-or-uncomment-lines
    "cl" 'evilnc-comment-or-uncomment-to-the-line
    "ll" 'evilnc-quick-comment-or-uncomment-to-the-line
    "cc" 'evilnc-copy-and-comment-lines
    "cp" 'evilnc-comment-or-uncomment-paragraphs
    "cr" 'comment-or-uncomment-region
    "cv" 'evilnc-toggle-invert-comment-line-by-line))


(use-package avy
  :ensure t
  :config
  (evil-leader/set-key
    "f" 'avy-goto-char-timer))


(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :config
  (defun my-create-newline-and-enter-sexp (&rest _ignored)
    "Open a new brace or bracket expression, with relevant newlines and indent."
    (newline)
    (indent-according-to-mode)
    (forward-line -1)
    (indent-according-to-mode))

  (sp-local-pair 'c++-mode "{" nil :post-handlers
                 '((my-create-newline-and-enter-sexp "RET")))

  (sp-local-pair '(emacs-lisp-mode lisp-interaction-mode) "'" nil :actions nil)

  (smartparens-global-mode 1))


(use-package evil-smartparens
  :ensure t
  :diminish evil-smartparens-mode
  :config
  (add-hook 'smartparens-mode-hook #'evil-smartparens-mode))


(use-package evil-matchit
  :ensure t
  :defer t
  :config
  (global-evil-matchit-mode 1))


(use-package projectile
  :ensure t
  :defer 10
  :diminish projectile-mode
  :config
  (projectile-mode t)
  (define-key evil-normal-state-map (kbd "C-p") 'projectile-find-file))


(use-package iedit
  :ensure t
  :diminish iedit-mode
  :defer t
  :bind (("C-c ;" . iedit-mode)))


(use-package ace-window
  :ensure t
  :config
  (setq aw-background nil)
  (global-set-key (kbd "M-p") 'ace-window)
  (defvar aw-dispatch-alist
    '((?x aw-delete-window " Ace - Delete Window")
      (?m aw-swap-window " Ace - Swap Window")
      (?n aw-flip-window)
      (?v aw-split-window-vert " Ace - Split Vert Window")
      (?b aw-split-window-horz " Ace - Split Horz Window")
      (?i delete-other-windows " Ace - Maximize Window")
      (?o delete-other-windows))
    "List of actions for `aw-dispatch-default'.")
  (setq aw-dispatch-always t))


(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)
         ("C-x G" . magit-status-with-prefix))
  :preface
  (defun magit-status-with-prefix ()
    (interactive)
    (let ((current-prefix-arg '(4)))
      (call-interactively magit-status)))
  :init
  (add-hook 'magit-mode-hook 'hl-line-mode))


(use-package xkcd
  :ensure t
  :config
  (evil-set-initial-state 'xkcd-mode 'emacs))


(use-package comint
  :bind* (:map
          comint-mode-map
          ("C-l" . comint-clear-buffer))
  ;; :map evil-insert-state-map
  ;; ([up] . comint-previous-input)
  ;; ([down] . comint-next-input))

  :preface
  (defun comint-clear-buffer ()
    (interactive)
    (let ((comint-buffer-maximum-size 0))
      (comint-truncate-buffer)))

  :config
  (setq comint-prompt-read-only t))


(use-package paradox
  :ensure t
  :init
  (setq paradox-automatically-star t)
  :config
  (setq paradox-github-token "4c8c530a7f1d9409419226de97494912c5e51578"))


(use-package aggressive-indent
  :ensure t
  :config
  (add-to-list
   'aggressive-indent-dont-indent-if
   '(and (derived-mode-p 'c++-mode)
         (null (string-match "\\([;{}]\\|\\b\\(if\\|for\\|while\\)\\b\\)"
                             (thing-at-point 'line)))))
  (add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
  (add-hook 'c++-mode-hook #'aggressive-indent-mode))


(use-package multiple-cursors
  :ensure t
  :bind (("C->" . mc/mark-next-like-this)
         ("C-<" . mc/mark-previous-like-this)
         ("C-c C-<" . mc/mark-all-like-this)
         ("C-S-c C-S-c" . mc/edit-lines)))


(use-package key-chord
  :ensure t
  :diminish key-chord-mode
  :config
  (key-chord-mode 1)
  (setq key-chord-two-keys-delay 0.5)
  (key-chord-define evil-insert-state-map "jj" 'evil-normal-state))


(use-package whitespace
  :disabled t
  :diminish (global-whitespace-mode
             whitespace-mode
             whitespace-newline-mode)
  :preface
  (defun my-other-delete-trailing-blank-lines ()
    "Deletes all blank lines at the end of the file, even the last one"
    (interactive)
    (save-excursion
      (save-restriction
        (widen)
        (goto-char (point-max))
        (delete-blank-lines)
        (let ((trailnewlines (abs (skip-chars-backward "\n\t"))))
          (if (> trailnewlines 1)
              (progn
                (delete-char trailnewlines)))))))
  :config
  (setq whitespace-style '(face lines-tail trailing))
  (add-hook 'prog-mode-hook 'whitespace-mode)
  ;; (add-hook 'LaTeX-mode-hook 'whitespace-mode)
  (add-hook 'before-save-hook 'delete-trailing-whitespace)
  (add-hook 'before-save-hook 'my-other-delete-trailing-blank-lines))


(use-package pdf-tools
  :disabled t
  :config
  (pdf-tools-install))


(use-package outline
  :commands outline-minor-mode
  :diminish outline-minor-mode
  :bind (("C-c C-o" . outline-minor-mode-prefix))
  :init
  (hook-into-modes #'outline-minor-mode
                   'emacs-lisp-mode
                   'LaTeX-mode-hook
                   'latex-mode-hook))

;; (load-file "~/.emacs.d/custom/org_settings.el")

;;-----------------------------------------------------------------------------
;;; C/C++ --- Google C style guide
;;;-----------------------------------------------------------------------------

(use-package electric-spacing
  :ensure t
  :diminish electric-spacing-mode
  :config
  (hook-into-modes #'electric-pair-mode
                   'cuda-mode-hook
                   'rust-mode-hook)
  (hook-into-modes #'electric-spacing-mode
                   'cuda-mode-hook
                   'c-mode-hook))


;; (use-package google-c-style
;;   :ensure t
;;   :defer t
;;   :init
;;   (add-hook 'c-mode-common-hook 'google-set-c-style)
;;   (add-hook 'c-mode-common-hook 'google-make-newline-indent)
;;   (use-package flycheck-google-cpplint
;;     :ensure t
;;     :config
;;     (add-to-list 'flycheck-checkers 'c/c++-googlelint)
;;     (custom-set-variables
;;      '(flycheck-c/c++-googlelint-executable "/usr/local/bin/cpplint.py")
;;      '(flycheck-googlelint-verbose "3")
;;      '(flycheck-googlelint-filter "-whitespace,+whitespace/braces")
;;      '(flycheck-googlelint-root "project/src")
;;      '(flycheck-googlelint-linelength "80"))))


(use-package company-c-headers
  :ensure t
  :config
  (add-to-list 'company-c-headers-path-system "/usr/include/c++/5/")
  (add-to-list 'company-backend 'company-c-headers))


(use-package irony
  :ensure t
  :preface
  (defun my-irony-mode-hook ()
    (define-key irony-mode-map [remap completion-at-point]
      irony-completion-at-point-async)
    (define-key irony-mode-map [remap complete-symbol]
      irony-completion-at-point-async))
  :config
  (progn
    (add-hook 'c++-mode-hook 'irony-mode)
    (add-hook 'c-mode-hook 'irony-mode)
    (add-hook 'objc-mode-hook 'irony-mode)
    (add-hook 'irony-mode-hook 'my-irony-mode-hook)
    (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)))


(use-package company-irony
  :ensure
  :config
  (add-to-list 'company-backends 'company-irony))


(use-package irony-eldoc
  :ensure
  :config
  (add-hook 'irony-mode-hook 'irony-eldoc))


(use-package flycheck-irony
  :ensure
  :config
  (add-hook 'flycheck-mode-hook #'flycheck-irony-setup))


;; (use-package company-irony-c-headers
;;   :ensure t
;;   :config
;;   (add-to-list 'company-backends '(company-irony-c-headers company-irony)))


;;;-----------------------------------------------------------------------------
;;; Python --- Yet Another Python Formatter
;;;        --- Sphinx documentation
;;;        --- Document strings
;;;        --- Realgud debugger
;;;        --- Python-Test
;;;        --- Anaconda for auto completion
;;;        --- Python-Env for virtual environments
;;;-----------------------------------------------------------------------------

(use-package jedi-core
  :ensure t
  :config
  (setq jedi:use-shortcuts 0)
  :bind* (:map python-mode-map
	       ("M-," . jedi:goto-definition)
	       ("M-." . jedi:goto-definition-pop-marker)
	       ("M-?" . jedi:show-doc)))


(use-package company-jedi
  :ensure t
  :config
  (defun my/python-mode-hook ()
    (add-to-list 'company-backends 'company-jedi))
  (add-hook 'python-mode-hook 'my/python-mode-hook))


(use-package py-yapf
  :disabled
  :ensure t
  :diminish py-yapf
  :config
  (add-hook 'python-mode-hook 'py-yapf-enable-on-save))


(use-package sphinx-doc
  :ensure t
  :diminish sphinx-doc-mode
  :config
  (hook-into-modes #'sphinx-doc-mode
                   'python-mode-hook))

(use-package python-docstring
  :ensure t
  :config
  (hook-into-modes #'python-docstring-mode
                   'Python-mode))

(use-package py-test
  :ensure t
  :config
  (add-hook 'python-mode-hook
            (lambda ()
              (local-set-key "\C-ca" 'pytest-all)
              (local-set-key "\C-cm" 'pytest-module)
              (local-set-key "\C-c." 'pytest-one)
              (local-set-key "\C-cd" 'pytest-directory)
              (local-set-key "\C-cpa" 'pytest-pdb-all)
              (local-set-key "\C-cpm" 'pytest-pdb-module)
              (local-set-key "\C-cp." 'pytest-pdb-one))))

(use-package anaconda-mode
  :ensure t
  :disabled t
  :diminish anaconda-eldoc-mode anaconda-mode eldoc-mode
  :config
  (progn (add-hook 'python-mode-hook 'anaconda-mode)
         (add-hook 'python-mode-hook 'anaconda-eldoc-mode)))


;; (use-package company-anaconda
;;   :ensure t
;;   :config
;;   (add-to-list 'company-backends '(company-anaconda :with company-capf)))


;; (use-package python
;;   :config
;;   (setq python-shell-interpreter "ipython"
;;         python-shell-interpreter-args "--simple-prompt -i --pylab"))


(use-package pycoverage
  :ensure t
  :config
  (defun my-coverage ()
    (interactive)
    (when (derived-mode-p 'python-mode)
      (progn
        (linum-mode)
        (pycoverage-mode)))))


(use-package realgud
  :disabled t
  :ensure t)


(use-package isend-mode
  :ensure t
  :config
  (setq isend-skip-empty-lines t)
  (add-hook 'isend-mode-hook 'isend-default-python-setup))


(use-package pyvenv
  :ensure t)

;;;-----------------------------------------------------------------------------
;;; Cython --- Cython-Mode      : Major mode for editing cython files
;;;        --- Flycheck-Cython  : checks errors in Cython mode
;;;-----------------------------------------------------------------------------

(use-package cython-mode
  :ensure t)


(use-package flycheck-cython
  :ensure t)

;;;-----------------------------------------------------------------------------
;;; LaTeX --- Write Good Mode   :  removes overused adjectives
;;;   --- Company auctex    :  supports auto completion
;;;   --- Company Math  :  auto completion for math symbols
;;;-----------------------------------------------------------------------------

(defun LaTeX-indent-item ()
  "Provide proper indentation for LaTeX.
\"\\item\" is indented `LaTeX-indent-level' spaces relative to
the the beginning of the environment.

  Continuation lines are indented either twice
  `LaTeX-indent-level', or `LaTeX-indent-level-item-continuation'
  if the latter is bound."
  (save-match-data
    (let* ((offset LaTeX-indent-level)
           (contin (or (and (boundp 'LaTeX-indent-level-item-continuation)
                            LaTeX-indent-level-item-continuation)
                       (* 2 LaTeX-indent-level)))
           (re-beg "\\\\begin{")
           (re-end "\\\\end{")
           (re-env "\\(itemize\\|\\enumerate\\|description\\)")
           (indent (save-excursion
                     (when (looking-at (concat re-beg re-env "}"))
                       (end-of-line))
                     (LaTeX-find-matching-begin)
                     (current-column))))
      (cond ((looking-at (concat re-beg re-env "}"))
             (or (save-excursion
                   (beginning-of-line)
                   (ignore-errors
                     (LaTeX-find-matching-begin)
                     (+ (current-column)
                        (if (looking-at (concat re-beg re-env "}"))
                            contin
                          offset))))
                 indent))
	    ((looking-at (concat re-end re-env "}"))
	     indent)
            ((looking-at "\\\\item")
             (+ offset indent))
            (t
             (+ contin indent))))))


(defcustom LaTeX-indent-level-item-continuation 4
  "Indentation of continuation lines for items in itemize."
  :group 'LaTeX-indentation
  :type 'integer)


;; Prevent Ispell from verifying some LaTeX commands
;;; code:
(defvar ispell-tex-skip-alists
  '("cite" "nocite"
    "includegraphics"
    "author" "affil"
    "ref" "eqref" "pageref"
    "label"))

;; Indentation with align-current in LaTeX environments
(defvar LaTeX-align-environments '("tabular" "tabular*"))

(defun un-urlify (fname-or-url)
  "A trivial function that replace a prefix of file:/// with just /."
  (if (string= (substring fname-or-url 0 8) "file:///")
      (substring fname-or-url 7)
    fname-or-url))

(defun evince-sync (file linecol &rest ignored)
  "Handle synctex signal from Evince."
  (let* ((fname (url-unhex-string (un-urlify file)))
	 (buf (find-buffer-visiting fname))
	 (line (car linecol))
	 (col (cadr linecol)))
    (if (null buf)
	(message "[Synctex]: %s is not opened..." fname)
      (switch-to-buffer buf)
      (goto-char (point-min))
      (forward-line (1- (car linecol)))
      (unless (= col -1)
	(move-to-column col)))))

(defvar *dbus-evince-signal* nil)

(defun enable-evince-sync ()
  "Enable synctex with Evince over DBus."
  (require 'dbus)
  (when (and
	 (eq window-system 'x)
	 (fboundp 'dbus-register-signal))
    (unless *dbus-evince-signal*
      (setf *dbus-evince-signal*
	    (dbus-register-signal
	     :session nil "/org/gnome/evince/Window/0"
	     "org.gnome.evince.Window" "SyncSource"
	     'evince-sync)))))


;;;-----------------------------------------------------------------------------
;;; ORG Mode--------------------------------------------------------------------
;;;-----------------------------------------------------------------------------
(use-package ox-reveal
  :ensure ox-reveal)

(setq org-reveal-root "http://cdn.jsdelivr.net/reveal.js/3.0.0/")
(setq org-reveal-mathjax t)

;;;-----------------------------------------------------------------------------
;;;-----------------------------------------------------------------------------
(use-package auctex
  :ensure t
  :mode ("\\.tex\\'" . latex-mode)
  :commands (latex-mode LaTeX-mode plain-tex-mode)
  :init
  (progn
    (add-hook 'LaTeX-mode-hook #'LaTeX-preview-setup)
    (add-hook 'LaTeX-mode-hook #'flyspell-mode)
    (add-hook 'LaTeX-mode-hook #'turn-on-reftex)
    (setq TeX-auto-save t
	  TeX-parse-self t
	  TeX-save-query nil
	  TeX-PDF-mode t)
    (setq-default TeX-master nil))
  :config
  (add-hook 'LaTeX-mode-hook #'latex-auto-fill)
  (add-hook 'LaTeX-mode-hook 'enable-evince-sync)
  (add-hook 'LaTeX-mode-hook (lambda () (setq compile-command "latexmk -pdf")))
  (add-hook 'LaTeX-mode-hook
	    (lambda ()
	      (require 'align)
	      (setq LaTeX-indent-environment-list
		    (mapcar (lambda (item)
		    	      (let ((env (car item)))
		    		(if (member env LaTeX-align-environments)
		    		    (list env 'align-current)
		    		  item)))
		    	    LaTeX-indent-environment-list))))
  (setq LaTeX-eqnarray-label "eq:"
	LaTeX-equation-label "eq:"
	LaTeX-figure-label "fig:"
	LaTeX-table-label "tab:"
	LaTeX-myChapter-label "chap:"
	TeX-newline-function 'reindent-then-newline-and-indent
	TeX-style-path
	'("style/"  "~/.emacs.d/site-lisp/auctex/style/")
	LaTeX-section-hook '(LaTeX-section-heading
			     LaTeX-section-title
			     LaTeX-section-toc
			     LaTeX-section-section
			     LaTeX-section-label)))

(load "auctex.el" nil t t)

(use-package latex-math-preview
  :ensure t
  :commands LaTeX-preview-setup
  :config
  (progn
    (setq-default preview-scale 1.4
		  preview-scale-function '(lambda ()
					    (* (/ 10.0 (preview-document-pt))
					       preview-scale)))))

(use-package reftex
  :diminish reftex-mode
  :commands turn-on-reftex
  :config
  (setq reftex-bibliography-commands '("bibliography" "nobibliography" "addbibresource"))
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (progn
    (setq reftex-plug-into-AUCTeX t)))


(use-package bibtex
  :mode ("\\.bib" . bibtex-mode)
  :config
  (progn
    (setq bibtex-align-at-equal-sign t)
    (add-hook 'bibtex-mode-hook (lambda () (set-fill-column 120)))))


(use-package latex-pretty-symbols
  :ensure t)


(use-package latex-preview-pane
  ;; :disabled t
  :ensure t
  :config
  (latex-preview-pane-enable))


(use-package company-math
  :ensure t
  :preface
  (defun my-latex-mode-setup ()
    (setq-local company-backends
                (append '((company-math-symbols-latex company-latex-commands))
                        company-backends)))
  :config
  (add-hook 'TeX-mode-hook 'my-latex-mode-setup))


;; (use-package company-auctex
;;   :ensure t
;;   :config
;;   (add-to-list 'company-backends 'company-auctex))


(use-package writegood-mode
             :ensure t
             :diminish writegood-mode
             :bind (("C-c g" . writegood-mode))
             :config
             (hook-into-modes #'writegood-mode
                              'text-mode-hook
                              'LaTeX-mode-hook))
;;;-----------------------------------------------------------------------------
;;; Bash --- auto completion for bash
;;;-----------------------------------------------------------------------------

(use-package bash-completion
  :ensure t
  :config
  (bash-completion-setup))



;;;-----------------------------------------------------------------------------
;;; Multiple Major Mode --- mmm-mode
;;;-----------------------------------------------------------------------------

(use-package mmm-mode
  :ensure t
  :config
  (setq mmm-global-mode 'maybe)
  (mmm-add-mode-ext-class 'python-mode "\\.mako\\'" 'cuda-mode))


;; (use-package mmm-mako
;;   :ensure t
;;   :config
;;   (add-to-list 'auto-mode-alist '("\\.mako\\'" . python-mode))
;;   (mmm-add-mode-ext-class 'python-mode "\\.mako\\'" 'mako))


;; (use-package cuda-mode
;;   :ensure t)

;;;-----------------------------------------------------------------------------
;;; Auto complete --company mode
;;;-----------------------------------------------------------------------------

(use-package company
  :ensure t
  :diminish company-mode
  :commands (compay-mode
             company-complete
             company-complete-common
             company-complete-common-or-cycle
             company-files
             company-dabbrev
             company-ispell
             company-c-headers
             company-jedi
             company-auctex)
  :init
  (setq company-minimum-prefix-length 20
        company-require-match 0
        company-selection-wrap-around t
        company-dabbrev-downcase nil
        company-tooltip-limit 15
        company-tooltip-align-annotations t
        company-idle-delay 0.1
        company-begin-commands '(self-insert-command))
  (eval-after-load 'company
    '(add-to-list 'company-backends '(company-files
                                      company-capf)))
  :bind (("C-c f" . company-files)
         ("C-c a" . company-dabbrev)
         ("C-c d" . company-ispell)
         ("<tab>" . indent-or-complete)
         ("TAB" . indent-or-complete)

         :map company-active-map
         ("C-a" . company-abort)
         ("<tab>" . indent-or-complete)
         ("TAB" . indent-or-complete)
         ("S-TAB" . company-select-previous)
         ("<return>" . nil)
         ("<backtab>" . company-select-previous))

  :config
  (defun indent-or-complete ()
    (interactive)
    (if (looking-at "\\_>")
        (company-complete-common-or-cycle)
      (indent-according-to-mode)))

  ;; (defun check-expansion ()
  ;;   (save-excursion
  ;;     (if (looking-at "\\_>") t
  ;;       (backward-char 1)
  ;;       (if (looking-at "\\.") t
  ;;         (backward-char 1)
  ;;         (if (looking-at "->") t nil)))))

  ;; (defun tab-indent-or-complete ()
  ;;   (interactive)
  ;;   (cond ((minibufferp) (minibuffer-complete))
  ;;         (t (indent-for-tab-command)
  ;;            (if (or (not yas-minor-mode) (null (do-yas-expand)))
  ;;                (if (check-expansion)
  ;;                    (progn (company-manual-begin)
  ;;                           (if (null company-candidates)
  ;;                               (progn (company-abort)
  ;;                                      (indent-for-tab-command)))))))))

  ;; (defun do-yas-expand ()
  ;;   (let ((yas/fallback-behavior 'return-nil))
  ;;     (yas/expand)))

  ;; (defun expand-snippet-or-complete-selection ()
  ;;   (interactive)
  ;;   (if (or (not yas-minor-mode) (null (do-yas-expand))
  ;;           (company-abort))
  ;;       (company-complete-common-or-cycle)))

  ;; ;; Add yasnippet support for all company backends
  ;; (defvar company-mode/enable-yas t
  ;;   "Enable yasnippet for every back-end")

  ;; (defun company-mode/backend-with-yas (backend)
  ;;   (if (or (not company-mode/enable-yas)
  ;;           (and (listp backend) (member 'company-yasnippet backend)))
  ;;       backend
  ;;     (append (if (consp backend) backend (list backend))
  ;;             '(:with company-yasnippet))))

  ;; (setq company-backends
  ;;       (mapcar #'company-mode/backend-with-yas company-backends))

  (global-company-mode))


(use-package company-quickhelp
  :ensure t
  :init
  (company-quickhelp-mode 1)
  (setq company-quickhelp-delay nil)
  :config
  (define-key company-active-map (kbd "M-h") #'company-quickhelp-manual-begin))


(use-package company-statistics
  :ensure t
  :config
  (company-statistics-mode))

;;;-----------------------------------------------------------------------------
;;; Rust --
;;;-----------------------------------------------------------------------------
(use-package rust-mode
  :ensure t
  :mode ("\\.rs\\'" . rust-mode)
  :config
  (add-hook 'rust-mode-hook
            (lambda ()
              (local-set-key (kbd "C-c <tab>") #'rust-format-buffer))))


(use-package cargo
  :ensure t
  :config
  (add-hook 'rust-mode-hook 'cargo-minor-mode))

(use-package racer
  :ensure t
  :init
  (setq racer-cmd "~/.cargo/bin/racer")
  :config
  (add-hook 'rust-mode-hook #'racer-mode)
  (add-hook 'racer-mode-hook #'eldoc-mode)
  (add-hook 'racer-mode-hook #'company-mode))

(define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)


(use-package flycheck-rust
  :ensure t)


(use-package rg
  :ensure)

;;;-----------------------------------------------------------------------------
;;; Haskell --
;;;-----------------------------------------------------------------------------
;; (use-package ghc
;;   :ensure t
;;   :config
;;   (autoload 'ghc-init "ghc" nil t)
;;   (autoload 'ghc-debug "ghc" nil t)
;;   (add-hook 'haskell-mode-hook (lambda () (ghc-init))))


(use-package intero
  :ensure t
  :bind
  (:map haskell-cabal-mode-map
        ("M-." . intero-goto-definition))
  :config
  (add-hook 'haskell-mode-hook 'intero-mode))


(use-package shm
  :ensure t
  :config
  (add-hook 'haskell-mode-hook 'structured-haskell-mode))

;; (use-package hs-lint
;;   :ensure t
;;   :config
;;   (defun my-haskell-mode-hook ()
;;     "Hlint for haskell"
;;     (local-set-key "\C-cl" 'hs-lint))
;;   (add-hook 'haskell-mode-hook 'my-haskell-mode-hook))


;; (use-package hlint-refactor
;;   :ensure
;;   :bind (("C-c b" . hlint-refactor-refactor-buffer)
;;   ("C-c r" . hlint-refactor-refactor-at-point))
;;   :config
;;   (add-hook 'haskell-mode-hook 'hlint-refactor-mode))


;; (use-package stack-mode
;;   :ensure t
;;   :config
;;   (add-hook 'haskell-mode-hook 'stack-mode))


;;;-----------------------------------------------------------------------------
;;; Company tool tip customization
;;;-----------------------------------------------------------------------------
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(flycheck-c/c++-googlelint-executable "/usr/local/bin/cpplint.py")
 '(flycheck-googlelint-filter "-whitespace,+whitespace/braces")
 '(flycheck-googlelint-linelength "120")
 '(flycheck-googlelint-root "project/src")
 '(flycheck-googlelint-verbose "3")
 '(package-selected-packages
   '(ox-reveal rg anaconda-mode ag fuzzy restart-emacs highlight-indentation yasnippet xkcd writegood-mode use-package swiper sphinx-doc soothe-theme solarized-theme smex shm screenshot realgud readline-complete ranger rainbow-mode racer pyvenv python-docstring pycoverage py-yapf py-test py-autopep8 projectile paradox muttrc-mode multiple-cursors multi-term moe-theme mmm-mako magit macrostep latex-preview-pane latex-pretty-symbols latex-math-preview key-chord isend-mode irony-eldoc intero iedit hlint-refactor golden-ratio ghc flycheck-rust flycheck-irony flycheck-haskell flycheck-cython flycheck-checkbashisms evil-smartparens evil-nerd-commenter evil-matchit evil-leader evil-easymotion electric-spacing dumb-jump cython-mode cyberpunk-theme cuda-mode company-statistics company-quickhelp company-math company-jedi company-irony-c-headers company-irony company-c-headers color-theme-sanityinc-tomorrow cargo beacon bash-completion auctex ample-theme airline-themes aggressive-indent ace-window))
 '(safe-local-variable-values
   '((whitespace)
     (whitespacemod)
     (whitespace-mode)
     (flycheck)
     (flyspell-mode)
     (flycheck-mode)))
 '(tool-bar-mode nil)
 '(tramp-syntax 'default nil (tramp)))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-preview ((t (:background "#241F36" :underline t))))
 '(company-preview-common ((t (:inherit company-preview :foreground "gray94"))))
 '(company-scrollbar-bg ((t (:background "white smoke"))))
 '(company-scrollbar-fg ((t (:background "black"))))
 '(company-tooltip ((t (:background "#DBD2BF" :foreground "#110F13"))))
 '(company-tooltip-annotation ((t (:foreground "#110F13"))))
 '(company-tooltip-common ((t nil)))
 '(company-tooltip-selection ((t (:background "#2B2A26" :foreground "#DBD2Bf")))))

;;;-----------------------------------------------------------------------------

(provide 'init)
;;;-----------------------------------------------------------------------------
;;; init ends here
;;;-----------------------------------------------------------------------------
